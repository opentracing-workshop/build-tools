#!/bin/bash

SCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $SCRIPTDIR/\_commons

set -e

if [ -z "$1" ]; then
 echo "Usage: $0 <namespace(-stag|-prod)>" >&2
 exit 1
fi

NAMESPACE=$1
TILLER=tiller
DEPLOY_USER=deploy

# This makes sure that only the helm client can access tiller (using a portforward).
# Otherwise, any pod in the network can contact the tiller port and let it install random yml files.
HELM_OVERRIDE="spec.template.spec.containers[0].command={/tiller,--listen=localhost:44134}"

# todo, make this a helm template ffs
cat > /tmp/${NAMESPACE}.yaml <<EOF
---
apiVersion: v1
kind: Namespace
metadata:
  name: $NAMESPACE

---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: $TILLER
  namespace: $NAMESPACE

---
kind: Role
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  namespace: $NAMESPACE
  name: $TILLER
rules:
- apiGroups: ["", "extensions", "apps", "policy"]
  resources: ["*"]
  verbs: ["*"]

---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: $TILLER
  namespace: $NAMESPACE
roleRef:
  kind: Role
  name: $TILLER
  apiGroup: rbac.authorization.k8s.io
subjects:
- kind: ServiceAccount
  name: $TILLER
  namespace: $NAMESPACE

---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: $DEPLOY_USER
  namespace: $NAMESPACE

---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: Role
metadata:
  name: $DEPLOY_USER
  namespace: $NAMESPACE
rules:
- apiGroups: [""]
  resources: ["pods/portforward"]
  verbs: ["create"]
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["list"]
- apiGroups: [""]
  resources: ["secrets"]
  verbs: ["create", "list", "delete"]

---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: RoleBinding
metadata:
  name: $DEPLOY_USER
  namespace: $NAMESPACE
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: $DEPLOY_USER
subjects:
- kind: ServiceAccount
  name: $DEPLOY_USER
  namespace: $NAMESPACE

---
EOF

wrap_kubectl apply -f /tmp/${NAMESPACE}.yaml
wrap_helm init --skip-refresh --upgrade --service-account $TILLER --tiller-namespace $NAMESPACE --history-max 5 --override="$HELM_OVERRIDE"
